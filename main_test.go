package main

import (
	"bytes"
	"os"
	"sort"
	"testing"
)

var (
	aTemperatures = []float64{3.53, 3.63, 4.63, 3.53, 3.66, 3.67}
	aAverage      = 3.78
	aMedian       = 3.65
	aMode         = []float64{3.53}

	bTemperatures = []float64{4.13, 4.15, 4.15, 3.88}
	bAverage      = 4.08
	bMedian       = 4.14
	bMode         = []float64{4.15}

	cTemperatures = []float64{3.96, 3.96, 3.95, 3.36, 3.36}
	cAverage      = 3.72
	cMedian       = 3.95
	cMode         = []float64{3.36, 3.96}
)

func assertPanic(t *testing.T, f func()) {
	defer func() {
		if r := recover(); r == nil {
			t.Errorf("The code did not panic")
		}
	}()
	f()
}

func assertOkay(t *testing.T, f func()) {
	defer func() {
		if r := recover(); r != nil {
			t.Errorf("The code panicked")
		}
	}()
	f()
}

func Float64Equal(a, b []float64) bool {
	if len(a) != len(b) {
		return false
	}
	sort.Float64s(a)
	sort.Float64s(b)
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

func TestCalculateAverage(t *testing.T) {
	if average := calculateAverage(aTemperatures); average != aAverage {
		t.Errorf("Expected=%v Actual=%v", aAverage, average)
	}

	if average := calculateAverage(bTemperatures); average != bAverage {
		t.Errorf("Expected=%v Actual=%v", bAverage, average)
	}

	if average := calculateAverage(cTemperatures); average != cAverage {
		t.Errorf("Expected=%v Actual=%v", cAverage, average)
	}
}

func TestCalculateMedian(t *testing.T) {
	if median := calculateMedian(aTemperatures); median != aMedian {
		t.Errorf("Expected=%v Actual=%v", aMedian, median)
	}

	if median := calculateMedian(bTemperatures); median != bMedian {
		t.Errorf("Expected=%v Actual=%v", bMedian, median)
	}

	if median := calculateMedian(cTemperatures); median != cMedian {
		t.Errorf("Expected=%v Actual=%v", cMedian, median)
	}
}

func TestCalculateMode(t *testing.T) {
	if mode := calculateMode(aTemperatures); !Float64Equal(mode, aMode) {
		t.Errorf("Expected=%v Actual=%v", aMode, mode)
	}

	if mode := calculateMode(bTemperatures); !Float64Equal(mode, bMode) {
		t.Errorf("Expected=%v Actual=%v", bMode, mode)
	}

	if mode := calculateMode(cTemperatures); !Float64Equal(mode, cMode) {
		t.Errorf("Expected=%v Actual=%v", cMode, mode)
	}
}

func TestParseArgs(t *testing.T) {
	oldArgs := os.Args
	defer func() { os.Args = oldArgs }()

	os.Args = []string{"cmd"}
	assertPanic(t, func() { parseArgs() })

	os.Args = []string{"cmd", "nonexistantfile"}
	assertPanic(t, func() { parseArgs() })

	os.Args = []string{"cmd", "test.json"}
	assertOkay(t, func() { parseArgs() })
}

func TestLoadJsonFile(t *testing.T) {
	assertPanic(t, func() { loadJsonFile("nonexistantfile") })
	assertOkay(t, func() { loadJsonFile("test.json") })
}

func TestParseJson(t *testing.T) {
	var bytes []byte

	// Invalid json
	bytes = []byte(`[{"some":1,"invalid":2,"json":3}`)
	assertPanic(t, func() { parseJson(bytes) })

	// Missing attribute; "timestamp"
	bytes = []byte(`[{"id": "a", "temperature": 3.65}]`)
	assertPanic(t, func() { parseJson(bytes) })

	bytes = []byte(`[{"id": "a", "timestamp": 12345, "temperature": 1.65}]`)
	data := parseJson(bytes)
	if temperatures, exists := data["a"]; exists {
		if !Float64Equal(temperatures, []float64{1.65}) {
			t.Errorf("Expected=[1.65], Actual=%v", temperatures)
		}
	} else {
		t.Errorf("Missing 'a' Id: %v", data)
	}
}

func TestProcessData(t *testing.T) {
	data := make(map[string][]float64)
	data["a"] = aTemperatures
	results := processData(data)

	if len(results) != 1 {
		t.Errorf("Expected 1 result, received %v", len(results))
	}

	result := results[0]

	if result.Id != "a" {
		t.Errorf("Expected=a Actual=%v", result.Id)
	}
	if result.Average != aAverage {
		t.Errorf("Expected=%v Received=%v", aAverage, result.Average)
	}
	if result.Median != aMedian {
		t.Errorf("Expected=%v Received=%v", aMedian, result.Median)
	}
	if !Float64Equal(result.Mode, aMode) {
		t.Errorf("Expected=%v Received=%v", aMode, result.Mode)
	}
}

func TestGenerateOutput(t *testing.T) {
	f := newFridge("a", aTemperatures)
	output := generateOutput([]fridge{f})
	if !bytes.Equal(output, []byte(`[{"id":"a","average":3.78,"median":3.65,"mode":[3.53]}]`)) {
		t.Errorf("Output doesn't match: %v", output)
	}
}
