package main

import (
	"encoding/json"
	"io/ioutil"
	"os"

	log "github.com/sirupsen/logrus"
)

// recording struct that json file data is marshalled into.
type recording struct {
	Id          *string  `json:"id"`
	Timestamp   *int     `json:"timestamp"`
	Temperature *float64 `json:"temperature"`
}

// Configured logging
func init() {
	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
	log.SetLevel(log.WarnLevel)
}

// parseArgs checks that one file is passed on the command line.
func parseArgs() (fileName string) {
	if len(os.Args) != 2 {
		log.Panic("Json file not provided")
	}

	fileName = os.Args[1]
	if _, err := os.Stat(fileName); err != nil {
		log.Panic("File not found")
	}

	log.Infof("Found file: %s", fileName)
	return
}

// loadJsonFile loads the Json file into a byte slice.
func loadJsonFile(fileName string) (bytes []byte) {
	log.Info("Loading file")

	jsonFile, err := os.Open(fileName)
	if err != nil {
		log.Panic(err)
	}
	defer jsonFile.Close()

	bytes, err = ioutil.ReadAll(jsonFile)
	if err != nil {
		log.Panic(err)
	}

	return
}

// parseJson parses the byte json data and checks that the required attributes
// exist on each of the recordings.
func parseJson(bytes []byte) (data map[string][]float64) {
	var recordings []recording
	if err := json.Unmarshal(bytes, &recordings); err != nil {
		log.Panic(err)
	}

	data = make(map[string][]float64)
	for _, recording := range recordings {
		if recording.Id == nil {
			log.Panic("Id is missing")
		}
		if recording.Timestamp == nil {
			log.Panic("Timestamp is missing")
		}
		if recording.Temperature == nil {
			log.Panic("Temperature is missing")
		}

		log.WithFields(log.Fields{
			"id":          *recording.Id,
			"timestamp":   *recording.Timestamp,
			"temperature": *recording.Temperature,
		}).Info("Found record")

		data[*recording.Id] = append(data[*recording.Id], *recording.Temperature)
	}

	return
}

// processData creates fridge structs which calculate the average, median, and
// mode using the provided temperatures.
func processData(data map[string][]float64) (results []fridge) {
	for id, temperatures := range data {
		log.Info("Calculating fridge stats")

		fridge := newFridge(id, temperatures)

		log.WithFields(log.Fields{
			"id":      fridge.Id,
			"average": fridge.Average,
			"median":  fridge.Median,
			"mode":    fridge.Mode,
		}).Info("Calculated fridge stats")

		results = append(results, fridge)
	}
	return results
}

// generateOutput Converts the fridge structs into json.
func generateOutput(results []fridge) (output []byte) {
	log.Info("Printing results")
	output, err := json.Marshal(results)
	if err != nil {
		log.Panic(err)
	}
	return
}

func main() {
	fileName := parseArgs()
	bytes := loadJsonFile(fileName)
	data := parseJson(bytes)
	results := processData(data)
	output := generateOutput(results)
	os.Stdout.Write(output)
}
