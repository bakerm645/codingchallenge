package main

import (
	"math"
	"sort"
)

// fridge struct contains the required metrics for each fridge.
type fridge struct {
	Id      string    `json:"id"`
	Average float64   `json:"average"`
	Median  float64   `json:"median"`
	Mode    []float64 `json:"mode"`
}

// newFridge constructs a fridge struct and uses the temperatures to calculate
// the required metrics.
func newFridge(id string, temperatures []float64) fridge {
	average := calculateAverage(temperatures)
	median := calculateMedian(temperatures)
	mode := calculateMode(temperatures)
	return fridge{id, average, median, mode}
}

// round rounds a float64 value to two decimal places without type conversion.
func round(value float64) float64 {
	return math.Round(value*100) / 100
}

// calcualteAverage calculates the average temperature of the fridge.
func calculateAverage(temperatures []float64) (average float64) {
	average = 0.0

	for _, temperature := range temperatures {
		average += temperature
	}

	average /= float64(len(temperatures))

	return round(average)
}

// calcualteMedian calcualtes the median temperature of the fridge. A different
// calculation is required depending on whether there are an even or odd number
// of temperatures.
func calculateMedian(temperatures []float64) (median float64) {
	sort.Float64s(temperatures)

	if len(temperatures)%2 == 0 {
		firstIndex := len(temperatures) / 2
		secondIndex := firstIndex - 1
		median = (temperatures[firstIndex] + temperatures[secondIndex]) / 2.0
	} else {
		median = temperatures[len(temperatures)/2]
	}

	return round(median)
}

// calculateMode calculates the mode temperature/s of the fridge. The frquency
// of each temperature and the max frequency can be tracked in one iteration.
func calculateMode(temperatures []float64) (mode []float64) {
	counter := make(map[float64]int)
	max := 0
	for _, temperature := range temperatures {
		counter[temperature]++
		if counter[temperature] > max {
			max = counter[temperature]
		}
	}

	for temperature, count := range counter {
		if count == max {
			mode = append(mode, round(temperature))
		}
	}

	return
}
