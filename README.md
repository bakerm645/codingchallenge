# SafetyCultue Coding Challenge

---

## Running:
### Requirements:
* go1.10.3

### Instructions:

```bash
usage: codingchallenge file
example: codingchallenge test.json
```

---

## Testing:
```bash
go test
```

---

## Assumptions:
* Program can run as a batch job.
* Not latency sensitive.
* Input / Output order doesn't matter.

---

## Design Decisions / Trade-Offs:
* Likely memory issues if json file is very large since the file is completely read into memory. If required, would be better to read the file / data as a stream.
* Initially written in Python, which while easier / faster, golang provides performance and safety (i.e. type checking) that makes it more reliable for production use.
* Method used to round to 2-decimal places could result in incorrect values due to floating point representation. Maybe better to use fmt.Sprintf?
